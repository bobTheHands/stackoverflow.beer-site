+++
title = ""
description = "A go package"
author = "Till Klocke"
date = "{{ .Date }}"
publishdate = "{{ .Date }}"
tags = ["Development", "Go"]
categories = ["Bastelei"]
comments = false
removeBlur = false
toc = false
packages = ["stackoverflow.beer/go/"]
repo = "https://codeberg.org/StackOverflowBrewery"
+++
