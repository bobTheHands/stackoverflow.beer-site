---
title: New Fashioned Robot Oil (WA)
description: 'The New Fashioned Robot Oil you already know, but aged on wood for several
    weeks. '
abv: 7.3
ibu: 37
og: 0
fg: 0
buGuRation: 0
beerColor: 0
untappdId: "4260692"
malts: []
hops: []
miscs: []
yeasts: []
lastBrewDate: ""
lastBatch: 0
author: StackOverflow Brewery
date: 2020-01-01T01:00:00+01:00
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
