---
title: New Fashioned Robot Oil
description: 'A Vanilla Milk Stout perfect for cold dark evening where you just want
    enjoy the complex aromas of vanilla and dark malts interacting with the sweetness
    of milk sugar. '
abv: 7.3
ibu: 37
og: 16.36541245671104
fg: 0
buGuRation: 0.55
beerColor: 33.7
untappdId: "4260691"
malts:
  - name: Caramel EXtra Dark
    supplier: BestMalz
    amount: 0.31
  - name: Munich
    supplier: BestMalz
    amount: 1.02
  - name: Roasted Barley
    supplier: BestMalz
    amount: 0.33
  - name: Vienna
    supplier: BestMalz
    amount: 4.9
  - name: Oats, Flaked
    supplier: ""
    amount: 0.2
hops:
  - name: Chinook
    origin: New Zealand
    amount: 34.8
    usage: Both
    alpha: 11.8
miscs:
  - name: Milchzucker
    amount: 250
    unit: g
    usage: Boil
  - name: Vanilleschoten
    amount: 8
    unit: items
    usage: Secondary
yeasts:
  - name: Nottingham Yeast
    laboratory: Lallemand (LalBrew)
    productId: ""
lastBrewDate: "2021-01-02T23:00:00Z"
lastBatch: 8
author: StackOverflow Brewery
date: 2021-01-02T23:00:00Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
