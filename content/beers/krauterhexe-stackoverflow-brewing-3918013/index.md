---
title: Kräuterhexe
description: 'Pale Ale with a refreshing Rosemary note. Perfect for your fine steak
    dinner or chilling at the water side during a heat wave. '
abv: 5.2
ibu: 24
og: 12.861598442176028
fg: 0
buGuRation: 0.45
beerColor: 5.4
untappdId: "3918013"
malts:
  - name: Caramel Hell
    supplier: BestMalz
    amount: 0.53
  - name: Pilsen Malt
    supplier: BestMalz
    amount: 5.26
hops:
  - name: Citra
    origin: US
    amount: 47
    usage: Both
    alpha: 12
miscs:
  - name: Rosmarin
    amount: 23.809
    unit: g
    usage: Boil
yeasts:
  - name: Voss Kveik
    laboratory: Lallemand (LalBrew)
    productId: ""
lastBrewDate: "2021-07-27T05:52:30Z"
lastBatch: 18
author: StackOverflow Brewery
date: 2021-07-27T05:52:30Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
