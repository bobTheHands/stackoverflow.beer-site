---
title: Brückenweizen
description: 'A Wheat Beer like the german "Brückenlockdown", less than you expect,
    but still does some damage. This is a light wheat beer, which is more bitter than
    expected. Also not alcoholic enough to get you hammered, but also not alcohol
    free. '
abv: 3.4
ibu: 7
og: 7.803636445626953
fg: 0
buGuRation: 0.23
beerColor: 3.5
untappdId: "4275234"
malts:
  - name: Heidelberg Wheat Malt
    supplier: BestMalz
    amount: 0.87
  - name: Smoked
    supplier: BestMalz
    amount: 0.122
  - name: Vienna
    supplier: BestMalz
    amount: 1.72
  - name: Wheat Malt
    supplier: BestMalz
    amount: 0.87
hops:
  - name: Mandarina Bavaria
    origin: Germany
    amount: 120
    usage: Both
    alpha: 8
miscs: []
yeasts:
  - name: Hornindal Kveik
    laboratory: Omega
    productId: OYL-091
lastBrewDate: "2021-03-24T23:00:00Z"
lastBatch: 10
author: StackOverflow Brewery
date: 2021-03-24T23:00:00Z
tags:
  - Bier
categories:
  - Bier
comments: false
removeBlur: false
draft: false

---
